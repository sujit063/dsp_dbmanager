CREATE TABLE DSP_UserRole
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name varchar(100) NOT NULL,
   roleType int NOT NULL,
   descriptaion varchar(100) DEFAULT NULL
);

CREATE TABLE DSP_IdentityType
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name varchar(100) NOT NULL,
   validation varchar(350) DEFAULT NULL
);

CREATE TABLE DSP_UserDetails
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name varchar(100) NOT NULL,
   userRoleId int NOT NULL,
   vendorId int,
   email varchar(200) NOT NULL,
   mobile varchar(15) NOT NULL,
   createdOn datetime NOT NULL DEFAULT NOW(),
   updatedOn datetime,
   isActive char(1) DEFAULT '1', 
   isDeleted  char(1) DEFAULT '0'
);

CREATE TABLE DSP_UserInfo
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   userId int NOT NULL,
   identityTypeId int NOT NULL,
   identityNum varchar(200) NOT NULL,
   identityDocs varbinary(256) NOT NULL,
   address varchar(500) NOT NULL,
   city varchar(50) NOT NULL,
   state varchar(50) NOT NULL,
   pinCode int NOT NULL
);

CREATE TABLE DSP_VenderRegistration
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name varchar(100) NOT NULL,
   identityTypeId int NOT NULL,
   identityNum varchar(200) NOT NULL,
   email varchar(200) NOT NULL,
   mobile varchar(15) NOT NULL,
   shopName varchar(500) NOT NULL,
   shopAddress varchar(500) NOT NULL,
   city varchar(50) NOT NULL,
   state varchar(50) NOT NULL,
   pinCode int NOT NULL,
   shopRegNum varchar(50) NOT NULL,
   identityDocs varbinary(256),
   shopRegDocs varbinary(256),
   photo varbinary(256),
   source varchar(50) NOT NULL,
   createdOn datetime NOT NULL DEFAULT NOW(),
   isVerified  char(1) DEFAULT '0',
   verifiedTime datetime,
   reason varchar(250),
   isDeleted  char(1) DEFAULT '0'
);

CREATE TABLE DSP_VendorDetails
(
   id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
   name varchar(100) NOT NULL,
   identityTypeId int NOT NULL,
   identityNum varchar(200) NOT NULL,
   email varchar(200) NOT NULL,
   mobile varchar(15) NOT NULL,
   shopName varchar(500) NOT NULL,
   shopAddress varchar(500) NOT NULL,
   city varchar(50) NOT NULL,
   state varchar(50) NOT NULL,
   pinCode int NOT NULL,
   shopRegNum varchar(50) NOT NULL,
   identityDocs varbinary(256),
   shopRegDocs varbinary(256),
   photo varbinary(256),
   createdOn datetime NOT NULL DEFAULT NOW(),
   updatedOn datetime,
   isActive  char(1) DEFAULT '1',
   isDeleted  char(1) DEFAULT '0'
);
