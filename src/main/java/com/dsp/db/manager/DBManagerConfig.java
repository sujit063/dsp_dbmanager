package com.dsp.db.manager;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
public class DBManagerConfig
{
	private static final Logger	OUT				= LoggerFactory.getLogger(DBManagerConfig.class);

	@Autowired
	public DataSource			dataSource		= null;

	@Value("${mybatis.mapper-locations}")
	private String				mapperLocation	= null;

	@PostConstruct
	public void init()
	{
		OUT.debug("-------INIT DBManagerConfig with mapperLocation:{} ----------", mapperLocation);
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception
	{
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource);
		factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocation));
		return factoryBean.getObject();
	}
}
