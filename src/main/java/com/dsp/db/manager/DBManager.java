package com.dsp.db.manager;

import java.util.List;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.dsp.common.interfaces.IModal;

@Repository
public class DBManager implements DBManagerInterface
{

	private static final Logger	OUT	= LoggerFactory.getLogger(DBManager.class);
	private SqlSessionFactory	sqlSessionFactory;

	public DBManager(SqlSessionFactory sqlSessionFactory)
	{
		this.sqlSessionFactory = sqlSessionFactory;
	}

	public <T> T selectOne(String statement) throws Exception
	{
		return get(statement, null);
	}

	public <T> T selectOne(String statement, Object parameter) throws Exception
	{
		return get(statement, parameter);
	}

	public <T> T selectOne(String statement, IModal parameter) throws Exception
	{
		return get(statement, parameter);
	}

	public <E> List<E> selectList(String statement) throws Exception
	{
		return this.getAsList(statement, null, 0, 0);
	}

	public <E> List<E> selectList(String statement, Object parameter) throws Exception
	{
		return this.getAsList(statement, parameter, 0, 0);
	}

	public <E> List<E> selectList(String statement, IModal parameter) throws Exception
	{
		return this.getAsList(statement, parameter, 0, 0);
	}

	public <E> List<E> selectList(String statement, Object parameter, int offset, int limit) throws Exception
	{
		return this.getAsList(statement, parameter, offset, limit);
	}

	public <E> List<E> selectList(String statement, IModal parameter, int offset, int limit) throws Exception
	{
		return this.getAsList(statement, parameter, offset, limit);
	}

	public int insert(String statement, IModal parameter) throws Exception
	{
		return this.doInsert(statement, parameter);
	}

	public int insert(String statement, Object parameter) throws Exception
	{
		return this.doInsert(statement, parameter);
	}

	public int update(String statement, Object parameter) throws Exception
	{
		return this.doUpdate(statement, parameter);
	}

	public int update(String statement, IModal parameter) throws Exception
	{
		return this.doUpdate(statement, parameter);
	}

	public int delete(String statement, Object parameter) throws Exception
	{
		return this.doDelete(statement, parameter);
	}

	public int delete(String statement, IModal parameter) throws Exception
	{
		return this.doDelete(statement, parameter);
	}

	public SqlSession getSession()
	{
		return this.sqlSessionFactory.openSession();
	}

	public SqlSession getBatchSession()
	{
		return this.sqlSessionFactory.openSession(ExecutorType.BATCH);
	}

	/**
	 * @param <E>
	 * @param statement
	 * @param parameter
	 * @param min
	 * @param max
	 * @return
	 * @throws Exception
	 */
	private <E> List<E> getAsList(String statement, Object parameter, int offset1, int limit1) throws Exception
	{

		int offset = offset1 > 0 ? offset1 : 0;
		int limit = limit1 > 0 ? limit1 : Integer.MAX_VALUE;
		RowBounds rowbound = new RowBounds(offset, limit);
		SqlSession openSession = null;
		OUT.debug("MY_BATIS:Select All started with statement:{}, offset:{}, limit:{}", statement, offset, limit);
		try
		{
			openSession = this.sqlSessionFactory.openSession();
			return openSession.selectList(statement, parameter, rowbound);
		}
		catch (Exception e)
		{
			if (null != openSession)
			{
				openSession.rollback();
			}
			throw e;
		}
		finally
		{
			if (null != openSession)
			{
				openSession.close();
			}
		}
	}

	/**
	 * @param <T>
	 * @param statement
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	private <T> T get(String statement, Object parameter) throws Exception
	{
		OUT.debug("MY_BATIS:Select One started with statement:{}", statement);
		SqlSession openSession = null;
		try
		{
			openSession = this.sqlSessionFactory.openSession();
			return openSession.selectOne(statement, parameter);
		}
		catch (Exception e)
		{
			if (null != openSession)
			{
				openSession.rollback();
			}
			throw e;
		}
		finally
		{
			if (null != openSession)
			{
				openSession.close();
			}
		}
	}

	/**
	 * @param statement
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	private int doInsert(String statement, Object parameter) throws Exception
	{
		OUT.debug("MY_BATIS:Insert started with statement:{}", statement);
		SqlSession openSession = null;
		try
		{
			openSession = this.sqlSessionFactory.openSession();
			int count = openSession.insert(statement, parameter);
			OUT.debug("MY_BATIS:Insert completed with statement:{}, count:{}", statement, count);
			return count;
		}
		catch (Exception e)
		{
			if (null != openSession)
			{
				openSession.rollback();
			}
			throw e;
		}
		finally
		{
			if (null != openSession)
			{
				openSession.close();
			}
		}
	}

	/**
	 * @param statement
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	private int doUpdate(String statement, Object parameter) throws Exception
	{
		OUT.debug("MY_BATIS:Update started with statement:{}", statement);
		SqlSession openSession = null;
		try
		{
			openSession = this.sqlSessionFactory.openSession();
			int count = openSession.update(statement, parameter);
			OUT.debug("MY_BATIS:Update completed with statement:{}, count:{}", statement, count);
			return count;
		}
		catch (Exception e)
		{
			if (null != openSession)
			{
				openSession.rollback();
			}
			throw e;
		}
		finally
		{
			if (null != openSession)
			{
				openSession.close();
			}
		}
	}

	/**
	 * @param statement
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	private int doDelete(String statement, Object parameter) throws Exception
	{
		OUT.debug("MY_BATIS:Delete started with statement:{}", statement);
		SqlSession openSession = null;
		try
		{
			openSession = this.sqlSessionFactory.openSession();
			int count = openSession.delete(statement, parameter);
			OUT.debug("MY_BATIS:delete completed with statement:{}, count:{}", statement, count);
			return count;
		}
		catch (Exception e)
		{
			if (null != openSession)
			{
				openSession.rollback();
			}
			throw e;
		}
		finally
		{
			if (null != openSession)
			{
				openSession.close();
			}
		}
	}

}
