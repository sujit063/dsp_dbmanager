package com.dsp.db.manager;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.dsp.common.interfaces.IModal;

public interface DBManagerInterface
{

	<T> T selectOne(String statement) throws Exception;

	<T> T selectOne(String statement, Object parameter) throws Exception;

	<T> T selectOne(String statement, IModal parameter) throws Exception;

	<E> List<E> selectList(String statement) throws Exception;

	<E> List<E> selectList(String statement, Object parameter) throws Exception;

	<E> List<E> selectList(String statement, Object parameter, int offset, int limit) throws Exception;

	<E> List<E> selectList(String statement, IModal parameter) throws Exception;

	<E> List<E> selectList(String statement, IModal parameter, int offset, int limit) throws Exception;

	int insert(String statement, IModal parameter) throws Exception;

	int insert(String statement, Object parameter) throws Exception;

	int update(String statement, Object parameter) throws Exception;

	int update(String statement, IModal parameter) throws Exception;

	int delete(String statement, Object parameter) throws Exception;

	int delete(String statement, IModal parameter) throws Exception;

	SqlSession getSession();

	SqlSession getBatchSession();

}
